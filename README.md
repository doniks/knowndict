# Known Dictionary

a dictionary app for Ubuntu Touch

Supports online and offline dictionaries. 

You can import dictionaries in the "stardict" format. Instructions are in the app.

## build

```
git clone https://gitlab.com/doniks/knowndict.git
cd knowndict 
clickable
```

## credit

previous repository:
https://code.launchpad.net/~fuxixi1991/knowndict/
